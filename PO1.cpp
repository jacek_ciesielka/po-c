#include <iostream>
#include <cstdlib>

class klasa_tab
{
	private:
		int tab[9];
	public:
		klasa_tab(int[9]);
		void drukuj();
		bool szukaj(int n);
};

void klasa_tab::drukuj()
{
	for(int j= 0; j < 9; j++)
		std::cout << tab[j] << " ";
}

klasa_tab::klasa_tab(int t[9])
{
	for(int i = 0; i < 9; i++)
		tab[i] = t[i];
		std::cout << " konstruktor" << std::endl;
}

bool klasa_tab::szukaj(int n)
{
	for(int i =0; i < 9; i++)
		if(tab[i] == n)
			return 1;
	return 0;
}

int main(void)
{
	int tablica[9] = {0, 1,2,3,4,5,6,7,8 };
	klasa_tab obiekt(tablica);
	obiekt.drukuj();
	int liczba;
	std::cout << " Podaj liczbe: "; std::cin >> liczba;
	bool x = obiekt.szukaj(liczba);
	if(x == 1)
	{
		std::cout << "Jest w tablicy. " << std::endl;
	}
	else
		std::cout << "Nie jest w tablicy. " << std::endl;
	return 0;
	
}