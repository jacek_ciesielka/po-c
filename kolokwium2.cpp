class dwojka_liczb{
    int a,b;
    public:
    dwojka_liczb(int x, int y){
        a=x;
        b=y;}

    virtual void drukuj();
}

class tensor2 : public dwojka_liczb
{
    int walencja;
    public
        tensor2(int,int,int);
        int zwroc_slad();
        void drukuj();
}

int main(){
    dwojka_liczb obiekt1(1,2);
    tensor2 obiekt2(3,4,5);
    dwojka_liczb *w;
    w -> &obiekt1;
    w -> drukuj();
    w = &obiekt2;
    w -> drukuj();
}